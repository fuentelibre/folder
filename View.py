import asyncio
import os
import sys
from gi.repository import Gio


class FilesView:

    def scan(self, path):
        self.cursor = Gio.File.new_for_path(path)
        self.path = path
        self.filelist = self.cursor.enumerate_children('standard::*,access::can-execute', 0, None)
        self.files = {}
        for item in self.filelist:
            if not item.get_is_hidden():
                gfile = self.filelist.get_child(item)
                try:
                    filename = item.get_name()
                except Exception as e:
                    print(e)
                    continue
                ext = filename[filename.rfind('.') + 1:] if '.' in filename else ''
                try:
                    thisEntry = {
                        'type': u'' + item.get_file_type().__repr__(),
                        'size': u'' + str(item.get_size()),
                        'path': u'' + str(gfile.get_uri()),
                        'mime': Gio.content_type_guess(filename=item.get_name(),
                                                       data=None),
                        'content': u'' + item.get_content_type(),
                        'executable': item.get_attribute_boolean('access::can-execute'),
                        'ext': ext
                    }
                    # thisEntry['color'] = self.getColor(thisEntry)
                    self.files[u'' + item.get_name()] = thisEntry
                except Exception as e:
                    print('ERROR: ', e)
        self.emit('scan', self.files)

    async def lch_dir(self, relpath):
        if self.term.check_child():
            self.term.write("")
        self.term.write("cd \"" + relpath + "\"\r")
        await asyncio.sleep(0.1)
        return self.path==relpath


    async def ch_dir(self, relpath):
        child = self.cursor.get_child(relpath)
        if child:
            newpath = child.get_path()
            self.scan(newpath)
            self.path = newpath
            return True

    def launch(self, name):
        if self.term.check_child():
            self.term.write("")
        self.term.write('xdg-open "' + name + '" &\r')
        return True

    def eval(self, code):
        return eval(code)

    def up_dir(self):
        if self.term.check_child():
            self.term.write("")
        self.term.write('cd ..\r')

    def list_files(self):
        return self.files

    def meta(self):
        self.emit('meta',  len(self.files))

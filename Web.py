#!/usr/bin/env python3
import os
import click
from Files import get_finder
from Terminal import Emulator


class JappyShell:
    '''
    Shell main abstraction.

    Should start all required services.

    Considering:
    - Glib loop
    - asyncio loop
    - forked processes

    And handle evented communications between them.
    '''

    def __init__(self):
        self.process_args()

    @click.command()
    @click.argument('path', type=click.Path(exists=True),
                    default=os.path.realpath(os.curdir))
    @click.option('--mode', default='window',
                  help='Mode of operation [window, server]')
    @click.option('--host', default='127.0.0.1',
                  help='Host (server mode)')
    @click.option('--port', default=8081,
                  help='Port (server mode)')
    def process_args(path, mode, port, host):
        '''
        A beginner's interactive shell.
        '''
        terminal = Emulator(path)
        import wuy
        get_finder(wuy.Server if mode == 'server' else wuy.Window)(term=terminal,
                                                                   path=path,
                                                                   port=port,
                                                                   host=host)


if __name__ == '__main__':
    if 'TERM' not in os.environ:
        os.environ['TERM'] = 'xterm-256color'
    JappyShell()

#!/usr/bin/env python3
import os
import asyncio
import sys
import struct
import fcntl
import termios
import traceback
import aionotify
from psutil import Process, CONN_LISTEN
from ptyprocess import PtyProcessUnicode

xonsh_color_style = 'default'
_buffer_size = 4096


class Emulator:

    def __init__(self, path, watch=True, read=True):
        self.loop = asyncio.get_event_loop()
        self.watcher = aionotify.Watcher()
        asyncio.ensure_future(self.monitor_work())
        self.watching = None
        self.children = []
        self.connections = []  # listening ports
        self.processing = False # ports

        self._connections = {}  # callbacks
        self.new_shell(path, watch=watch, read=read)

    def connect(self, eventname, callback):
        callbacks = self._connections.get(eventname)
        if callbacks:
            callbacks.append(callback)
            self._connections[eventname] = callbacks
        else:
            self._connections[eventname] = [callback]

    def emit(self, eventname, data=None):
        callbacks = self._connections.get(eventname)
        if callbacks:
            for fun in callbacks:
                fun(data)

    def monitor_changes(self, e=None):
        if self.curdir != self.watching:
            if self.watching:
                self.watcher.unwatch(self.watching)
            self.watcher.watch(self.curdir, flags=aionotify.Flags.CREATE | aionotify.Flags.DELETE)
            self.watching = self.curdir

    async def monitor_work(self):
        await self.watcher.setup(self.loop)
        while True:
            event = await self.watcher.get_event()
            self.loop.call_soon(lambda: self.emit('update', self.curdir))
            await asyncio.sleep(0.2)
        self.watcher.close()

    def new_shell(self, path=None, watch=True, read=True):
        if not path:
            path = os.path.dirname(os.path.abspath( __file__ ))
        self.pty = PtyProcessUnicode.spawn([os.environ.get('SHELL') or '/bin/bash'],
                                           cwd=path)
        os.set_blocking(self.pty.fileno(), False)
        self.curdir = path or os.path.realpath(os.curdir)
        self.monitor_changes()
        self.connect('chdir', self.monitor_changes)
        if watch:
            watcher = asyncio.get_child_watcher()
            watcher.add_child_handler(self.pty.pid, self.stop_term)

        self.buffer = ''
        self.buffered = True
        self.__out_buffer = ''
        # fcntl.fcntl(self.pty, fcntl.F_SETFL, os.O_NONBLOCK & ~termios.ECHO)
        # loop.add_signal_handler(signal.SIGPIPE, self.stop_term)
        # asyncio.set_child_watcher(self.stop_term)

        # get the current settings se we can modify them
        newattr = termios.tcgetattr(self.pty)

        # set the terminal to uncanonical mode and turn off
        # input echo.
        # newattr[3] &= ~termios.ICANON & ~termios.ECHO

        # # don't handle ^C / ^Z / ^\
        # newattr[6][termios.VINTR] = b'\x00'
        # newattr[6][termios.VQUIT] = b'\x00'
        # newattr[6][termios.VSUSP] = b'\x00'

        # set our new attributes
        termios.tcsetattr(self.pty, termios.TCSADRAIN, newattr)

        # store the old fcntl flags
        self.oldflags = fcntl.fcntl(self.pty, fcntl.F_GETFL)
        # make the PTY non-blocking
        fcntl.fcntl(self.pty, fcntl.F_SETFL, self.oldflags | os.O_NONBLOCK)

        fcntl.fcntl(self.pty, fcntl.F_SETFD, fcntl.FD_CLOEXEC)

        if read:
            self.loop.add_reader(self.pty, self.read_stream)

    def stop_term(self, pid, returncode):
        self.loop.remove_reader(self.pty)
        watcher = asyncio.get_child_watcher()
        watcher.remove_child_handler(self.pty.pid)
        if pid:
            self.new_shell()
            self.emit('exit')

    def register(self, finder):
        '''
        Registers a UI object
        (must have 'write' method or else we disable reader)
        '''
        if hasattr(finder, 'write'):
            self.output = finder.write
            if self.buffer:
                finder.write(self.buffer)
        else:
            self.loop.remove_reader(self.pty)
        self.buffer = ''

    def read_stream(self):
        data = ''
        while True:
            try:
                chunk = self.pty.read(_buffer_size)
            except UnicodeDecodeError:
                break
            except EOFError:
                break
            except OSError:
                sys.stdout.write(traceback.format_exc())
                sys.stdout.flush()
                chunk = None
            if not chunk:
                break
            else:
                data += chunk

        if hasattr(self, 'output'):
            if self.buffered:
                self.push_to_buffer(data)
            else:
                self.output(data)
        else:
            self.buffer += data

    def check_child(self):
        try:
            children = Process(self.pty.pid).children(recursive=True)
        except psutil.NoSuchProcess:
            children = []
        return children

    def check_port(self, children):
        connections = []
        for p in children:
            if p.is_running():
                # connections += self.check_port(p.children())
                connections += [x[3][1] for x in p.connections() if x.status == CONN_LISTEN]
        return connections

    def process_ports(self, children, wait=1, limit=20):
        if children and self.processing is True:
            return
        self.processing = True
        # Detect listening server
        if children is None:
            children = self.check_child()
        connections = self.check_port(children)
        if connections:
            print('LISTEN-PORT', connections)
            self.emit('listen-port', {'port': connections})
            self.processing = False
        else:
            if wait <= limit:
                self.loop.call_later(wait, self.process_ports, None, wait * 2, limit)
                print('Waiting another ', wait, ' seconds.')
            else:
                self.processing = False
        self.connections = connections

    def on_enter_key(self):
        try:
            curdir = os.readlink('/proc/' + str(self.pty.pid) + '/cwd')
        except FileNotFoundError:
            self.output('No process running.')

        # Detect process launch
        children = self.check_child()
        if not self.children and children:
            self.emit('spawn-child', children)
            print('SPAWN', children)
            self.process_ports(children)
        elif self.children and not children:
            self.emit('die-child', self.children)
            print('DIE', self.children)
            if self.connections and not self.check_port(children):
                self.emit('close-ports')
        self.children = children

        if curdir != self.curdir:
            self.curdir = curdir
            self.emit('chdir', {'olddir': self.curdir,
                                'newdir': curdir})

    def write(self, data):
        self.pty.write(data)
        self.pty.flush()
        if '\r' in data or '\n' in data or '' in data:
            self.loop.call_later(0.1, self.on_enter_key)

    def flush_buffer(self):
        self.output(self.__out_buffer)
        self.__out_buffer = ''

    def push_to_buffer(self, data):
        if self.__out_buffer:
            self.__out_buffer += data
        else:
            self.__out_buffer = data
            self.loop.call_later(0.01, self.flush_buffer)

    def set_winsize(self, row, col, xpix=0, ypix=0):
        winsize = struct.pack("HHHH", col, row, xpix, ypix)
        fcntl.ioctl(self.pty, termios.TIOCSWINSZ, winsize)

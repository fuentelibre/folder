FROM debian:buster-slim
COPY . /app
WORKDIR /app
RUN apt update && apt install --no-install-recommends -y python3-wheel python3-psutil python3-setuptools python3-gi gobject-introspection libgtk-3-0 python3-pip && pip3 install -r /app/requirements.txt && rm -rf /var/lib/apt/lists
RUN adduser --disabled-password --gecos '' jappy && chmod a+r /app -R && chmod a+w /app/vues/*
USER jappy
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENTRYPOINT ["./Web.py", "--mode", "server"]
CMD ["/home/jappy"]

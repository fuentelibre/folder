#!/usr/bin/env python3

import asyncio
import os
import sys
import gbulb
import gi
import json
import signal
from Terminal import Emulator

gi.require_version('Gtk', '3.0')
gi.require_version('Vte', '2.91')
ZOOM_STEP = 1024

from gi.repository import Gtk, Gio, Vte, Gdk, GLib, Pango

from WuyWidget import get_widget
webvue = get_widget('files_view')

class JappyShellApp(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self, application_id="org.fuentelibre.happyshell",
                                 flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.connect("activate", self.on_activate)
        self.connect("open", self.open)
        self.curdir = "."

    def open(self, app, files, hint, t):
        print('open')
        for item in files:
            print(item.get_uri())

    def on_activate(self, data=None):
        if not self.get_windows():
            self.shell = Emulator(watch=True, read=False)
            self.shell.connect('exit', self.new_term)
            self.shell.connect('chdir', lambda e: self.window.set_title(e['newdir']))
            webvue.hook_terminal(self.shell)

            self.loop = asyncio.get_event_loop()
            self.window = Gtk.Window(type=Gtk.WindowType.TOPLEVEL)
            self.window.set_default_size(800, 700)
            self.split_pos = 0.5
            self.window.set_title(os.path.realpath(os.curdir))
            self.terminal = Vte.Terminal()
            self.terminal.set_font(Pango.FontDescription("IBM Plex Mono"))
            self.terminal.set_pty(Vte.Pty.new_foreign_sync(self.shell.pty.fileno()))
            #self.terminal.watch_child(self.shell.pty.pid)
            if self.shell.buffer:
                self.terminal.feed(self.shell.buffer)
            # self.terminal.spawn_sync(Vte.PtyFlags.DEFAULT, os.environ["HOME"],
            #                         ['/bin/bash'], [], GLib.SpawnFlags. DO_NOT_REAP_CHILD,
            #                         None, None)
            self.terminal.set_allow_bold(True)
            fg_color = Gdk.RGBA()
            fg_color.parse('DimGray')
            bg_color = Gdk.RGBA()
            bg_color.parse('BlanchedAlmond')
            self.terminal.set_colors(fg_color, bg_color, [])
            self.terminal.connect('key_press_event', self.copy_or_paste)
            #self.terminal.connect('child-exited', self.new_term)
            vb = Gtk.Paned.new(Gtk.Orientation.VERTICAL)
            vb.set_wide_handle(True)
            vb.pack1(webvue, True, True)
            vb.pack2(self.terminal, True, True)
            self.window.add(vb)
            self.window.show_all()
            self.window.set_icon_from_file('web/files.svg')
            self.add_window(self.window)
            self.vb = vb
            self.window.connect('destroy', lambda *args: asyncio.get_event_loop().stop)
            self.loop.call_soon(self.fix_size)
            # self.shell.register(self)
            self.terminal.grab_focus()
        else:
            print('Command mode')

    def new_term(self, term, n=None):
        self.shell.stop_term(0,0)
        self.shell.new_shell(read=False)
        self.loop.call_soon(lambda:self.terminal.set_pty(Vte.Pty.new_foreign_sync(self.shell.pty.fileno())))
        #self.terminal.watch_child(self.shell.pty.pid)

    def fix_size(self):
        height = self.window.get_size().height
        self.vb.set_position(int(height * self.split_pos))

    def emit(self, signal, data):
        if signal == 'event':
            event = json.loads(data)
            if 'shutdown' in event:
                self.shell.stop_term()
                asyncio.get_event_loop().stop()
                # loop = asyncio.get_event_loop()
                #loop.call_soon(self.shell.stop_term)
                #loop.call_soon(loop.stop)
                #GLib.idle_add(lambda *args: Gtk.main_quit())
                #GLib.idle_add(lambda *args: sys.exit(0))
                sys.exit(0)
            elif 'newdir' in event:
                self.window.set_title(event['newdir'])
        else:
            super().emit(event, data)

    def copy_or_paste(self, widget, event):
        """Decides if the Ctrl+Shift is pressed, in which case returns True.
        If Ctrl+Shift+C or Ctrl+Shift+V are pressed, copies or pastes,
        acordingly. Return necesary so it doesn't perform other action,
        like killing the process, on Ctrl+C.
        """
        control_key = Gdk.ModifierType.CONTROL_MASK
        shift_key = Gdk.ModifierType.SHIFT_MASK
        numlock = Gdk.ModifierType.MOD2_MASK
        if event.type == Gdk.EventType.KEY_PRESS:
            # print(event.keyval)
            # print(dir(event))
            if event.state == shift_key | control_key or \
               event.state == shift_key | control_key | numlock:  # both shift  and control
                if event.keyval == 67:  # that's the C key
                    self.terminal.copy_clipboard()
                    return True
                elif event.keyval == 86:  # and that's the V key
                    self.terminal.paste_clipboard()
                    return True
                elif event.keyval == 43:  # and that's the + key
                    self.__zoom_in_cb()
                    return True
            elif event.state == control_key or \
               event.state == control_key | numlock:  # control
                if event.keyval == 45:  # and that's the - key
                    self.__zoom_out_cb()
                    return True
            elif event.keyval == 65293:  # Enter
                self.loop.call_later(0.1, self.shell.on_enter_key)

    def _zoom(self, step):
        vt = self.terminal
        font_desc = vt.get_font()
        font_desc.set_size(font_desc.get_size() + step)
        vt.set_font(font_desc)

    def __zoom_out_cb(self):
        self._zoom(ZOOM_STEP * -1)

    def __zoom_in_cb(self):
        self._zoom(ZOOM_STEP)

if __name__ == "__main__":
    app = JappyShellApp()
    gbulb.install(gtk=True)
    GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, signal.SIGINT, app.quit)
    loop = asyncio.get_event_loop()
    loop.run_forever(application=app)
    #app.run(sys.argv)

#!/usr/bin/env python3

import click
import os
import asyncio
import aionotify
import wuy


class index(wuy.Server):
    """
    <iframe id='viewer'></iframe>
    <script>
    var viewer = document.getElementById('viewer')
    async function set_view(name) {
        viewer.contentDocument.body.outerHTML = await wuy.render_file(name)
    }
    async function load_index() {
        viewer.contentDocument.body.outerHTML = await wuy.load_index()
    }
    document.addEventListener('init', function (e) {
                            wuy.on('update', set_view)
                            load_index()
                        }
                    )
    </script>
    <style>
    html, body, iframe { width: 100%; height: 100%; margin: 0px; border: 0px; }
    </style>
    """

    def __init__(self, path, mode, content):
        self.path = path
        self.content = content
        self.loop = asyncio.get_event_loop()
        self.watcher = aionotify.Watcher()
        asyncio.ensure_future(self.monitor_work())
        super().__init__(log=True, host='127.0.0.1',
                         port='9081')

    async def monitor_work(self):
        await self.watcher.setup(self.loop)
        self.watcher.watch(self.path, flags=aionotify.Flags.MODIFY | aionotify.Flags.CREATE | aionotify.Flags.DELETE)
        while True:
            event = await self.watcher.get_event()
            print (event)
            self.emit('update', event.name or event.alias)
            await asyncio.sleep(0.2)
        self.watcher.close()

    def render_file(self, name):
        if os.path.isfile(name):
            with open(name) as f:
                output = f.read()
            return output
        else:
            return self.content

    async def load_index(self):
        return self.content


if __name__ == '__main__':
    @click.command()
    @click.argument('path', type=click.Path(exists=True),
                    default=os.path.realpath(os.curdir))
    @click.option('--mode', default='window',
                  help='Mode of operation [window, server]')
    def run(path, mode):
        if os.path.isfile(path):
            with open(path) as f:
                content = f.read()
        else:
            content = 'Watching ' + path
        index(path, mode, content)

    run() # run index run !

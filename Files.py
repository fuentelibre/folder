#!/usr/bin/env python3
import os
import vbuild
import sys
from View import FilesView

remotes = ["https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.css",
           "https://cdn.jsdelivr.net/npm/vue/dist/vue.js",
           "https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.js",
           "https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/addons/fit/fit.js",
           "https://cdn.jsdelivr.net/npm/split.js@1.5.10/dist/split.min.js"]

template = """
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.css"></link>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.js"></script>
<script src="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/addons/fit/fit.js"></script>
<script src="https://cdn.jsdelivr.net/npm/split.js@1.5.10/dist/split.min.js"></script>

<script src="wuy.js"></script>
<!-- HERE -->
<div id="app">
    <div id="container">
        <files_view></files_view>
        <viewer_iframe></viewer_iframe>
    </div>
    <terminal_emulator></terminal_emulator>
</div>
<script>
Terminal.applyAddon(fit)
new Vue({el:"#app"})
</script>
<style>
    body,html { height:100%; margin: 0px }
    #app { height: 100%; display: flex; flex-direction: column }
    #container { display: flex; flex-direction: row }
    .gutter {
        background-color: #eee;

        background-repeat: no-repeat;
        background-position: 50%;
        border: 1px solid #C0C0BD;
    }

    .gutter.gutter-horizontal {
        cursor: col-resize;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAIklEQVQoU2M4c+bMfxAGAgYYmwGrIIiDjrELjpo5aiZeMwF+yNnOs5KSvgAAAABJRU5ErkJggg==');
    }

    .gutter.gutter-vertical {
        cursor: row-resize;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAFAQMAAABo7865AAAABlBMVEVHcEzMzMzyAv2sAAAAAXRSTlMAQObYZgAAABBJREFUeF5jOAMEEAIEEFwAn3kMwcB6I2AAAAAASUVORK5CYII=');
    }

</style>
"""


def get_finder(interface):

    class index(interface, FilesView):

        def __init__(self, term=None, path=None, port=8080, host='127.0.0.1', *args, **kwargs):
            if not path:
                path = os.path.dirname(os.path.abspath(__file__))
            else:
                os.chdir(path)
            self.files = {}
            self.term = term
            term.connect('chdir', lambda e: self.emit('chdir', e['newdir']))
            term.connect('listen-port', lambda e: self.emit('listen-port', e))
            term.connect('close-ports', lambda e: self.emit('close-ports'))
            term.connect('spawn-child',
                         lambda childs:
                         self.emit('spawn-child', [child.pid for child in childs]))
            super().__init__(files=self.files, log=False, host=host,
                             port=port, path=path, *args, **kwargs)

        def _render(self, path):
            v = vbuild.render(os.path.join(sys.path[0], "vues/*"))
            for url in remotes:
                basename = os.path.basename(url)
                if os.path.isfile(os.path.join(sys.path[0], 'web/' + basename)):
                    global template
                    template = template.replace(url, basename)
            return template.replace("<!-- HERE -->", str(v))

        def scan_curdir(self):
            self.scan(self.term.curdir)
            return self.term.curdir

        def register(self, flags=None):
            self.term.register(self)
            self.term.connect('chdir', lambda data: self.scan(data['newdir']))
            self.term.connect('update', lambda curdir: self.scan(curdir))

        def term_data_in(self, data):
            self.term.write(data)

        def write(self, data):
            if data:
                self.emit('data', data)

        def set_winsize(self, row, col, xpix=0, ypix=0):
            if self.term:
                self.term.set_winsize(row, col, xpix, ypix)

    return index


if __name__ == '__main__':
    from sh import wget

    os.chdir('web')

    # Download deps
    for url in remotes:
        print('Downloading ', url)
        wget(url)

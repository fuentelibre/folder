import os
import sys
import wuy
import vbuild
import pty
import signal
import asyncio
from View import FilesView


class Widget(FilesView, wuy.Server):
    """
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.css"></link>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/xterm.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/xterm@3.8.0/dist/addons/fit/fit.js"></script>
    <script src="wuy.js"></script>
    <!-- HERE -->
    <div id="app">
    <!-- COMPONENT -->
    </div>
    <script>
    Terminal.applyAddon(fit)
    new Vue({el:"#app"})
    </script>
    <style>body,html,#app { height:100% }</style>
    """

    def __init__(self, component, evt_pipe=None, *args, **kwargs):
        self.component = component
        self.files = {}
        # TODO: Make generic event and callback mechanism
        self.term = os.fdopen(evt_pipe, "w")  # XXX: For sending terminal commands
        '''
        self.termpipe = os.fdopen(evt_pipe, "w")  # XXX: For sending terminal commands

        class writeFlush:

            def __init__(self, pipe):
                self.termpipe = pipe

            def write(self, data):
                self.termpipe.write(data)
                self.termpipe.flush()

        self.term = writeFlush(self.termpipe)
        '''
        super().__init__(files=self.files, *args, **kwargs)

    def _render(self, path):
        content = self.__doc__
        v = vbuild.render(os.path.join(sys.path[0], "vues/*"))
        content = content.replace("<!-- HERE -->", str(v))
        content = content.replace("<!-- COMPONENT -->", '<%s></%s>'
                                  % (self.component, self.component))
        return content

def get_widget(vue_file, term=None):
    pout, pin = pty.openpty()
    pid = os.fork()
    if pid == 0:
        try:
            Widget(vue_file, evt_pipe=pin, log=False, port=8081)
        except Exception as e:
            print(e)
        finally:
            sys.exit(0)
    else:
        global Gtk
        import gi
        # import gbulb
        gi.require_version('WebKit2', '4.0')
        gi.require_version('Gtk', '3.0')
        from gi.repository import Gtk
        from gi.repository import WebKit2, GLib
        # gbulb.install(gtk=True)

        class WuyWidget(WebKit2.WebView):
            size = (800, 400)

            def __init__(self, pid, term=None):
                self.pid = pid
                super().__init__()
                self.hook_events()
                self.set_hexpand(True)
                self.set_vexpand(True)

            def hook_events(self):
                # self.set_size_request(*self.size)
                s = self.get_settings()
                s.set_enable_developer_extras(True)
                self.connect('destroy', self.teardown)
                self.load_uri('http://localhost:8081/Widget.html')

            def read_event(self):
                data = ''
                while True:
                    try:
                        chunk = self.evt_pipe.read(1024)
                    except EOFError:
                        break
                    except OSError:
                        sys.stdout.write(traceback.format_exc())
                        sys.stdout.flush()
                        chunk = None
                    if not chunk:
                        break
                    else:
                        data += chunk
                if self.term:
                    self.term.write(data)

            def hook_terminal(self, term):
                self.term = term
                self.evt_pipe = os.fdopen(pout, 'r')  # For events from Wuy
                os.set_blocking(pout, False)
                asyncio.get_event_loop().add_reader(self.evt_pipe, self.read_event)
                term.connect('chdir', self.on_chdir)
                term.connect('spawn-child', self.on_spawn)
                term.connect('update', self.on_update)

            def teardown(self, some):
                os.kill(self.pid, signal.SIGTERM)

            def on_chdir(self, data):
                self.run_javascript("wuy.scan('" + data['newdir'] + "');")

            def on_update(self, path):
                self.run_javascript("wuy.scan('" + path + "');")

            def on_spawn(self, childs):
                pids = [child.pid for child in childs]
                self.run_javascript(
                    "document.dispatchEvent( new CustomEvent('spawn-child', {detail:"
                    + str(pids) + "}) );")

        return WuyWidget(pid=pid, term=term)


if __name__ == '__main__':
    vue = get_widget()
    w = Gtk.Window()
    w.add(vue)
    w.show_all()
    # w.connect('destroy', lambda *args: asyncio.get_event_loop().stop())
    w.connect('destroy', lambda *args: sys.exit(0))
    Gtk.main()
    # asyncio.get_event_loop().run_forever()
